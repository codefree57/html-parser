<?php declare(strict_types=1);

require 'vendor/autoload.php';

use App\Exception\FileNotFound;
use App\Exception\FileRequired;
use App\Html;

$html = new Html();

if (!isset($argv[1])) {
    throw new FileRequired('File is required');
}

try {
    $html->loadHtmlFile($argv[1]);
    print_r($html->getTags());
} catch (FileNotFound $e) {
    echo $e->getMessage().PHP_EOL;
}
