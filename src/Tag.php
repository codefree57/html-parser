<?php declare(strict_types=1);

namespace App;

use App\Config\File;
use App\Exception\FileNotFound;

class Tag implements TagInterface
{
    private array $tags = [];

    /**
     * @throws FileNotFound
     */
    public function loadHtmlTags()
    {
        $path = File::getHtmlTagsPath();

        if (!file_exists($path)) {
            throw new FileNotFound('File not found `' . $path . '`');
        }

        $json = file_get_contents($path);

        $this->tags = json_decode($json) ?? [];
    }

    public function IsHtmlTag(string $tag): bool
    {
        return in_array($tag, $this->tags);
    }
}
