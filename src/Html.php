<?php declare(strict_types=1);

namespace App;

use App\Exception\FileNotFound;

class Html implements HtmlInterface
{
    private string $htmlContent;

    /**
     * @throws FileNotFound
     */
    public function loadHtmlFile(string $path)
    {
        if (!file_exists($path)) {
            throw new FileNotFound('File not found `' . $path . '`');
        }

        $this->htmlContent = file_get_contents($path);
    }

    /**
     * @throws FileNotFound
     */
    public function getTags(): array
    {
        $tag = new Tag();
        $tag->loadHtmlTags();
        $tagFound = [];

        preg_match_all('/<\s*(\w+)\s*[a-zA-Z0-9\s\=\_\-"]*\s*\/?>/mi', $this->htmlContent, $re);

        foreach($re[1] as $item) {
            if ($tag->IsHtmlTag($item)) {
                $tagFound[] = $item;
            }
        }

        return $tagFound;
    }
}
