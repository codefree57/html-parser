<?php declare(strict_types=1);

namespace App\Config;

class File
{
    private const HTML_TAGS =  '/storage/html-tags.json';

    public static function getHtmlTagsPath(): string
    {
        return dirname(__FILE__, 3) . self::HTML_TAGS;
    }
}
