<?php declare(strict_types=1);

namespace App;

interface HtmlInterface
{
    public function loadHtmlFile(string $path);
    public function getTags(): array;
}
