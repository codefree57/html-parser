<?php declare(strict_types=1);

namespace App;

interface TagInterface
{
    public function loadHtmlTags();
    public function IsHtmlTag(string $tag);
}
